package com.formacionbdi.springboot.app.productos.models.service;

import java.util.List;

import com.springboot.app.commons.models.entity.Producto;

public interface IProductoService {

	public List<Producto> findAll();
	public Producto findById(Long id);
	
	/*Agregar un Producto*/
	public Producto save(Producto producto);
	
	/*Eliminar Producto*/
	public void deleteById(Long id);
}
